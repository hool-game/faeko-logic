import cards from '@hool/cards'
import Faeko from '../index.js'

describe("Is the trick complete?", () => {
  // Some test data
  let players = JSON.stringify([
    {
      bid: 3,
      hand: [
        { suit: 'C', rank: 'J'},
        { suit: 'C', rank: '3'},
        { suit: 'D', rank: 'Q'},
        { suit: 'H', rank: '9'},
        { suit: 'S', rank: 'J'},
      ],
    },
    {
      bid: 4,
      hand: [
        { suit: 'C', rank: '7'},
        { suit: 'D', rank: 'A'},
        { suit: 'H', rank: 'Q'},
        { suit: 'H', rank: 'J'},
        { suit: 'H', rank: '2'},
      ],
    },
    {
      bid: 3,
      hand: [
        { suit: 'D', rank: 'A'},
        { suit: 'D', rank: '4'},
        { suit: 'H', rank: 'A'},
        { suit: 'H', rank: '3'},
        { suit: 'S', rank: '8'},
      ],
    },
  ])

  let played = [
    { suit: 'C', rank: 'J'},
    { suit: 'C', rank: '7'},
    { suit: 'D', rank: '4'},
  ]

  test("ordinary trick is complete", () => {
    let faeko = new Faeko({
      players: JSON.parse(players),
      tricks: new cards.Tricks()
    })

    faeko.data.tricks.loadArray([
      [
        { player: 0, card: { suit: 'C', rank: 'J'} },
        { player: 1, card: { suit: 'C', rank: '7'} },
        { player: 2, card: { suit: 'D', rank: '4'} },
      ],
    ])

    expect(faeko.isTrickComplete()).toBe(true)
  })

  test("trick completed with different starting point", () => {
    let faeko = new Faeko({
      players: JSON.parse(players),
      tricks: new cards.Tricks()
    })

    faeko.data.tricks.loadArray([
      [
        { player: 2, card: { suit: 'D', rank: '4'} },
        { player: 0, card: { suit: 'C', rank: 'J'} },
        { player: 1, card: { suit: 'C', rank: '7'} },
      ],
    ])

    expect(faeko.isTrickComplete()).toBe(true)
  })

  test("incomplete trick", () => {
    let faeko = new Faeko({
      players: JSON.parse(players),
      tricks: new cards.Tricks()
    })

    faeko.data.tricks.loadArray([
      [
        { player: 0, card: { suit: 'C', rank: 'J'} },
        { player: 1, card: { suit: 'C', rank: '7'} },
      ],
    ])

    expect(faeko.isTrickComplete()).toBe(false)
  })

  test("incomplete trick with different starting point", () => {
    let faeko = new Faeko({
      players: JSON.parse(players),
      tricks: new cards.Tricks()
    })

    faeko.data.tricks.loadArray([
      [
        { player: 2, card: { suit: 'D', rank: '4'} },
        { player: 0, card: { suit: 'C', rank: 'J'} },
      ],
    ])

    expect(faeko.isTrickComplete()).toBe(false)
  })

  test("trick finished with 1 inactive player", () => {
    let faeko = new Faeko({
      players: JSON.parse(players),
      tricks: new cards.Tricks()
    })

    faeko.data.players[1].inactive = true

    faeko.data.tricks.loadArray([
      [
        { player: 0, card: { suit: 'C', rank: 'J'} },
        { player: 2, card: { suit: 'D', rank: '4'} },
      ],
    ])

    expect(faeko.isTrickComplete()).toBe(true)
  })

  test("player left in middle of trick", () => {
    let faeko = new Faeko({
      players: JSON.parse(players),
      tricks: new cards.Tricks()
    })

    faeko.data.players[1].inactive = true

    faeko.data.tricks.loadArray([
      [
        { player: 0, card: { suit: 'C', rank: 'J'} },
        { player: 1, card: { suit: 'C', rank: '7'} },
        { player: 2, card: { suit: 'D', rank: '4'} },
      ],
    ])

    expect(faeko.isTrickComplete()).toBe(true)
  })

})


describe("Is flipping allowed? (open game)", () => {

  // Set up the scene
  let hands = [
    [
      { suit: 'C', rank: 'J'},
      { suit: 'C', rank: '3'},
      { suit: 'D', rank: 'Q'},
      { suit: 'H', rank: '9'},
      { suit: 'S', rank: 'J'},
    ],
    [
      { suit: 'C', rank: '7'},
      { suit: 'D', rank: 'A'},
      { suit: 'H', rank: 'Q'},
      { suit: 'H', rank: 'J'},
      { suit: 'H', rank: '2'},
    ],
    [
      { suit: 'D', rank: 'A'},
      { suit: 'D', rank: '4'},
      { suit: 'H', rank: 'A'},
      { suit: 'H', rank: '3'},
      { suit: 'S', rank: '8'},
    ],
  ]

  let faeko = new Faeko({
    players: [
      {
        bid: 3,
        hand: [...hands[0]],
      },
      {
        bid: 4,
        hand: [...hands[1]],
      },
      {
        bid: 3,
        hand: [...hands[2]],
      },
    ],
    tricks: new cards.Tricks()
  })

  test('no flipping before the game starts', () => {
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('no flipping for first half of game', () => {

    // Play one round
    faeko.play(0, hands[0][0])
    faeko.play(1, hands[1][0])
    faeko.play(2, hands[2][0])

    // Flip should be disabled
    expect(faeko.isFlipAllowed()).toBeFalsy()

    // Play another round
    faeko.play(0, hands[0][1])
    faeko.play(1, hands[1][1])
    faeko.play(2, hands[2][1])

    // Flip should still be disabled
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('no flipping when trick is incomplete (before half mark)', () => {

    // Play two cards in the third round
    faeko.play(0, hands[0][2])
    faeko.play(1, hands[1][2])

    // Flip shouldn't be enabled yet
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('flip when halfway trick completes', () => {

    // Complete the round. Now it can flip!
    faeko.play(2, hands[2][2])
    expect(faeko.isFlipAllowed()).toBeTruthy()
  })

  test('no flipping when trick is incomplete (after half mark)', () => {

    // Start the fourth round
    faeko.play(0, hands[0][3])
    faeko.play(1, hands[1][3])

    // It should stil be incomplete
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('flipping on last trick', () => {

    // Complete the fourth round
    faeko.play(2, hands[2][3])

    // Finish the fifth and final round
    faeko.play(0, hands[0][4])
    faeko.play(1, hands[1][4])
    faeko.play(2, hands[2][4])

    // The winner should still be able to flip
    expect(faeko.isFlipAllowed()).toBeTruthy()
  })

})


describe("Is flipping allowed? (closed game)", () => {

  // Set up the scene
  let hands = [
    [
      { suit: 'C', rank: 'J'},
      { suit: 'C', rank: '3'},
      { suit: 'D', rank: 'Q'},
      { suit: 'H', rank: '9'},
      { suit: 'S', rank: 'J'},
    ],
    [
      { suit: 'C', rank: '7'},
      { suit: 'D', rank: 'A'},
      { suit: 'H', rank: 'Q'},
      { suit: 'H', rank: 'J'},
      { suit: 'H', rank: '2'},
    ],
    [
      { suit: 'D', rank: 'A'},
      { suit: 'D', rank: '4'},
      { suit: 'H', rank: 'A'},
      { suit: 'H', rank: '3'},
      { suit: 'S', rank: '8'},
    ],
  ]

  let faeko = new Faeko({
    players: [
      {
        bid: 3,
        hand: null,
      },
      {
        bid: 4,
        hand: [...hands[1]],
      },
      {
        bid: 3,
        hand: null,
      },
    ],
    tricks: new cards.Tricks()
  })

  test('no flipping before the game starts', () => {
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('no flipping for first half of game', () => {

    // Play one round
    faeko.play(0, hands[0][0], false)
    faeko.play(1, hands[1][0])
    faeko.play(2, hands[2][0], false)

    // Flip should be disabled
    expect(faeko.isFlipAllowed()).toBeFalsy()

    // Play another round
    faeko.play(0, hands[0][1], false)
    faeko.play(1, hands[1][1])
    faeko.play(2, hands[2][1], false)

    // Flip should still be disabled
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('no flipping when trick is incomplete (before half mark)', () => {

    // Play two cards in the third round
    faeko.play(0, hands[0][2], false)
    faeko.play(1, hands[1][2])

    // Flip shouldn't be enabled yet
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('flip when halfway trick completes', () => {

    // Complete the round. Now it can flip!
    faeko.play(2, hands[2][2], false)
    expect(faeko.isFlipAllowed()).toBeTruthy()
  })

  test('no flipping when trick is incomplete (after half mark)', () => {

    // Start the fourth round
    faeko.play(0, hands[0][3], false)
    faeko.play(1, hands[1][3])

    // It should stil be incomplete
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('flipping on last trick', () => {

    // Complete the fourth round
    faeko.play(2, hands[2][3], false)

    // Finish the fifth and final round
    faeko.play(0, hands[0][4], false)
    faeko.play(1, hands[1][4])
    faeko.play(2, hands[2][4], false)

    // The winner should still be able to flip
    expect(faeko.isFlipAllowed()).toBeTruthy()
  })

})


describe("Is flipping allowed? (dataless)", () => {

  // Set up the scene

  // Here we'll use a deck object, because our logic will
  // inspect it to decide when the flip happens.
  let deck = new cards.Deck(['2','3','4','5'])

  // Deal out the cards, this will give 5 cards each
  let hands = deck.deal(3).hands

  let faeko = new Faeko({
    // Custom deck with less ranks
    deck: deck,
    players: [
      {
        bid: 3,
        hand: null,
      },
      {
        bid: 4,
        hand: null,
      },
      {
        bid: 3,
        hand: null,
      },
    ],
    tricks: new cards.Tricks()
  })

  test('no flipping before the game starts', () => {
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('no flipping for first half of game', () => {

    // Play one round
    faeko.play(0, hands[0][0], false)
    faeko.play(1, hands[1][0], false)
    faeko.play(2, hands[2][0], false)

    // Flip should be disabled
    expect(faeko.isFlipAllowed()).toBeFalsy()

    // Play another round
    faeko.play(0, hands[0][1], false)
    faeko.play(1, hands[1][1], false)
    faeko.play(2, hands[2][1], false)

    // Flip should still be disabled
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('no flipping when trick is incomplete (before half mark)', () => {

    // Play two cards in the third round
    faeko.play(0, hands[0][2], false)
    faeko.play(1, hands[1][2], false)

    // Flip shouldn't be enabled yet
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('flip when halfway trick completes', () => {

    // Complete the round. Now it can flip!
    faeko.play(2, hands[2][2], false)
    expect(faeko.isFlipAllowed()).toBeTruthy()
  })

  test('no flipping when trick is incomplete (after half mark)', () => {

    // Start the fourth round
    faeko.play(0, hands[0][3], false)
    faeko.play(1, hands[1][3], false)

    // It should stil be incomplete
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('flipping on last trick', () => {

    // Complete the fourth round
    faeko.play(2, hands[2][3], false)

    // Finish the fifth and final round
    faeko.play(0, hands[0][4], false)
    faeko.play(1, hands[1][4], false)
    faeko.play(2, hands[2][4], false)

    // The winner should still be able to flip
    expect(faeko.isFlipAllowed()).toBeTruthy()
  })

})

describe("Is flipping allowed? (re-flip)", () => {

  // Set up the scene

  // Here we'll use a deck object, because our logic will
  // inspect it to decide when the flip happens.
  let deck = new cards.Deck(['2','3','4','5'])

  // Deal out the cards, this will give 5 cards each
  let hands = deck.deal(3).hands

  let faeko = new Faeko({
    // Custom deck with less ranks
    deck: deck,
    players: [
      {
        bid: 3,
        hand: null,
      },
      {
        bid: 4,
        hand: null,
      },
      {
        bid: 3,
        hand: null,
      },
    ],
    tricks: new cards.Tricks()
  })

  test('no flipping before the game starts', () => {
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })

  test('flipping enabled on the middle trick', () => {
    // Play one round
    faeko.play(0, hands[0][0], false)
    faeko.play(1, hands[1][0], false)
    faeko.play(2, hands[2][0], false)

    // Play another round
    faeko.play(0, hands[0][1], false)
    faeko.play(1, hands[1][1], false)
    faeko.play(2, hands[2][1], false)

    // Play the third round
    faeko.play(0, hands[0][2], false)
    faeko.play(1, hands[1][2], false)
    faeko.play(2, hands[2][2], false)

    // Flipping should be enabled
    expect(faeko.isFlipAllowed()).toBeTruthy()
  })

  test('re-flipping disabled after first flip', () => {
    faeko.data.tricks.flip()
    expect(faeko.isFlipAllowed()).toBeFalsy()
  })
})
