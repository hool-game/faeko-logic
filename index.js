import cards from '@firtoska/cards'
import Firtoska from '@firtoska/logic'

export default function Faeko(data) {

  // Inherit behaviour and functions from Firtoska
  Firtoska.apply(this)

  // Get the data (could be a datastore or just
  // a vanilla object
  this.data = data

  /**
   * Is flip allowed?
   *
   * Detects whether the current player is allowed to flip or
   * not.
   */
  this.isFlipAllowed = () => {

    // Make sure there are tricks!
    if (!this.data.tricks?.length) return false

    // Count how many tricks have been made so far
    let tricksCount = this.data.tricks.length

    // Make sure the trick is complete
    if (!this.isTrickComplete()) return false

    // Make sure the trick is not already flipped!
    if (this.data.tricks.flipped) return false

    // Get all the hands we know about, and count their length
    // Consider the hands of active players only
    let knownHands = [...this.data.players]
      .filter(p => !!p.hand && !p.inactive)
      .map(p => p.hand.length)

    // If we know at least one player's hand, the logic is simple
    if (knownHands.length) {

      // Count the maximum number of cards a player is holding
      let maxCards = Math.max(...knownHands)

      // Make sure that the number of tricks is *more* than the
      // number of unplayed cards a player has! If it's more,
      // that means we've crossed the halfway mark.
      return tricksCount > maxCards

    } else {

      // If we don't have information on any hands, we have to take
      // a guess by looking at the number of players and dividing
      // the total number of cards (default: 52) by the total number
      // of players. We then divide that estimated length by two to
      // get the halfway mark.
      let halfwayMark = Math.floor(
        (this.data.deck?.cards?.length || 52)
        / this.data.players.length
        / 2 // divide it all by two to get the halfway mark
      )

      return tricksCount > halfwayMark
    }

    // We've taken care of all conditions, so no code will run after
    // this.
    console.eror('If you can see this message, then something is very wrong!')
  }


  /**
   * Get Active Dealers
   *
   * This function returns the active dealers
   * array. Based on this array, the turn will
   * give us the current dealer for the particular
   * game.
   */

  this.getActiveDealers = () => {
    return this.data.dealers
      .map((p, i) => ({ ...p, index: i }))
      .filter((p) => !p.inactive);
  };

  /**
 * Get Turn
 *
 * This function inspects the game state and figures out
 * whose turn it is. There could be multiple players (eg.
 * in the case of bidding), so we return the result as an
 * array.
 */
  this.getTurn = () => {
    let turn = {
      type: null,
      players: [],
    }

    // If there is no player data, it's nobody's turn
    if (!this.data?.players?.length) {
      return turn
    }

    // From now on, we process only active players
    let activePlayers = this.getActivePlayers()
    let activeDealers = this.getActiveDealers()

    // If there are no active players, or only one
    // player, then it's game over!
    if (!activePlayers.length) {
      return turn
    }

    // If there are no hands, we inspect the tricks
    // to figure out the status.
    console.log("THREE POSIBILE ", this.data.tricks, activePlayers.every(p =>
      !p.hand || !p.hand.length))
    if (
      activePlayers.every(p =>
        !p.hand || !p.hand.length)
    ) {
      // Three possibilities:
      if (!this.data.tricks.length) {

        // Either there are no tricks and the game
        // hasn't started yet...

        return turn

      } else if (this.isTrickComplete()) {

        // ...or the final trick is complete but we're
        // waiting for it to be taken/flipped...

        turn.type = 'take',
          turn.players.push(
            this
              .data
              .tricks
              .getWinningCard(this.data.tricks.length) // it's 1-indexed
              .player
          )

        return turn
      } else if (
        this.data.tricks.length >= 2
        && this.data.tricks[this.data.tricks.length - 1].length == 0
        && this.isTrickComplete(this.data.tricks.length - 2)
      ) {

        // ...or the penultimate trick is complete and
        // we have an empty trick at the end, which means
        // the trick's been taken and the game's over.
        //
        // (The empty trick at the end is to signify
        // the 'taken'ness)

        turn.type = 'score'
        return turn
      }

      // If it's neither of those, it means the game is
      // actually in progress but we just can't see the
      // cards (eg. if we played out all our cards and are
      // waiting for our opponents to play cards that we
      // can't see). In this case, we don't return but
      // continue processing the logic...
    }

    // If the bids aren't full, those who haven't
    // bid must do so
    if (
      !activePlayers.every(p =>
        typeof (p.bid) == 'number' ||
        p.bid == true)
    ) {
      turn.type = 'bid'
      turn.players = (activePlayers
        .filter(p =>
          typeof (p.bid) != 'number' && p.bid != true) // empty bids
        .map(p => p.index)) // return indexes

      return turn
    }

    console.log("TESTING :", !activePlayers.every(p => p.hand?.length == 0))

    // If bids are complete but hands still have cards,
    // it must be time to take or play tricks.
    if (!activePlayers.every(p => p.hand?.length == 0)) {
      turn.type = 'take'

      // If it's the first card ever, the dealer starts
      if (
        !this.data.tricks?.length ||
        (
          this.data.tricks.length == 1 &&
          this.data.tricks[0].length == 0
        )
      ) {
        // If the dealer is not defined, the dealer
        // is 0 (that is, the first player).
        console.log("tjhis dealer ", this.data.dealer)
        if (typeof (this.data.dealer) != 'number') {
          this.data.dealer = 0
        } else {
          // If the activeDealers array has some active
          // dealer then we have to get it from there.
          // Will return the index of the dealer.
          this.data.dealer = activeDealers.findIndex(
            (dealerObj) => dealerObj.dealer
          );
        }

        turn.type = 'card' // there's nothing to take!
        turn.players = [this.data.dealer]
        return turn
      }

      // If it's not the first card ever, we should
      // inspect the current trick to find out what's
      // going on
      let currentTrick = this.data.tricks[this.data.tricks.length - 1]

      // If this trick is full, the winner can choose to take
      if (
        activePlayers.every(p =>
          currentTrick.find(t =>
            t.player == p.index))
      ) {
        let thisWinner = (this.data.tricks
          .getWinningCard(this.data.tricks.length - 1 + 1) // 1-indexed
          .player)

        // Make sure it's an active player
        let nextPlayer = this.nextActivePlayer(thisWinner)

        turn.type = 'take'
        turn.players = [nextPlayer]
        return turn
      }

      // If it's the first card of another trick, then
      // the winner of the previous trick starts
      if (currentTrick.length == 0) {
        let previousWinner = (this.data.tricks
          .getWinningCard(this.data.tricks.length - 2 + 1) // 1-indexed
          .player)

        // Make sure it's an active player
        let nextPlayer = this.nextActivePlayer(previousWinner)

        turn.type = 'card'
        turn.players = [nextPlayer]
        return turn
      }

      // If it's not a first card at all, we have it easy:
      // just follow the person who played earlier
      let lastPlayer = currentTrick[currentTrick.length - 1].player

      let nextPlayer = this.nextActivePlayer(
        (lastPlayer + 1)
        % this.data.players.length
      )

      turn.type = 'card'
      turn.players = [nextPlayer]
      return turn
    }

    // If all the cards have been played out, it's the end of
    // the game so there's no question of playing, let alone
    // whose turn it is to play!
    turn.type = 'score'
    return turn
  }

  /**
 *  getDealScore
 *
 *  this function returns the deal score of all players.
 *  dealscore is based on tricks they won and bid they made
 *
 */
  this.getDealScore = () => {
    // first findout how many players in the game including the inactive players
    let playerCount = this.data.players.length

    // get the trick score
    let trickScore = this.getTrickScore()

    let lastTrick = this.data.tricks[this.data.tricks.length - 2].getTrickArray()

    // find players who played the last trick.
    // because we are calculating score only for players who played the last trick
    let playersPlayed = new Map(lastTrick.map(t => [t.player, true]));

    // find the players who made the contract bid
    let playersWon = trickScore.map((s, i) => {
      if (s == this.data.players[i].bid) return true
      else return false
    })

    let dealScore = Array(playerCount).fill(0)
    // if no one is made the contract means return score of zero
    if (playersWon.every(f => !f)) return dealScore

    /**
     *  calculate the offset score
     *
     *  dealScore is based on players who didn't made the contract
     *  for example in four player , if two player made the contract
     *  another two didn't made. so players who made contract get points
     *  from those didn't made which is based on bid difference.
     *  bid difference is diff between what they bid and what they got.
     */

    let initialScore = 0
    let totalScore = playersWon.reduce((accumulator, currentValue, index) => {
      //only if they played the last trick
      if (!currentValue && playersPlayed.get(index)) {
        //difference between bid they made and tricks they got
        let bidDiff = Math.abs(
          Number(trickScore[index]) - Number(this.data.players[index].bid)
        )

        return accumulator + (bidDiff * (bidDiff + 1)) / 2
      } else {
        return accumulator
      }
    }, initialScore)

    //if totalscore is zero means everyone made the contract except the disconnected players . so winners get bonus of 5
    if (totalScore == 0) {
      dealScore.forEach((_, i) => {
        if (playersPlayed.get(i)) {
          dealScore[i] = 5;
        }
      });
    }

    //change the score players who made the contract
    if (totalScore) {
      playersWon.forEach((flag, i) => {
        if (flag && playersPlayed.get(i)) dealScore.splice(i, 1, totalScore);
      });
    }

    //finally return the dealscore
    return dealScore
  }
}
